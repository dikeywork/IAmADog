from spider import Spider
from send import Send
import time

TIME_TO_DO = '08:30:00'  # 发送时间点
MSG_SUFFIX = '\n来自你的小可爱——Baldwin'  # 短信后缀
SOURCE_URL = 'http://www.1juzi.com/new/150542.html'  # 情话资源地址
SEND_TO_TEL = '+8618436354553'  # 女神的手机号

SEND_TO_ME = '舔狗，给女神发的短信已经用光了，快来更新！！！'
MY_TEL = '+8618436354553'  # 舔狗手机号


def doSend():
    index = 0  # 下一条短信的下标
    sentenceList = Spider.getSentenceList(SOURCE_URL)  # 情话列表

    while True:
        # 刷新
        time_now = time.strftime("%H:%M:%S", time.localtime())
        # 此处设置每天定时的时间
        if time_now == TIME_TO_DO:
            # 需要执行的动作
            # 判断当前list有没有用光
            if index >= len(sentenceList):
                # 用光了就短信通知我
                Send.sendSMSMsg(SEND_TO_ME, MY_TEL)
                # 跳出
                break

            # 给女神发短信
            content = sentenceList[index] + MSG_SUFFIX
            Send.sendSMSMsg(content, SEND_TO_TEL)

            # 下标加一
            index += 1

            # 因为以秒定时，所以暂停2秒，使之不会在1秒内执行多次
            time.sleep(2)


# 主程序入口
if __name__ == '__main__':
    doSend()
